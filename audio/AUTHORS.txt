################################################################################
# This file is contributors list of this directory and its sub directories. 
# See <repository root>/AUHTORS.txt for all contributors.
#-------------------------------------------------------------------------------
# このファイルはこのディレクトリおよびサブディレクトリの貢献者リストです。
# すべての貢献者は、<リポジトリルート>/AUTHORS.txtをご覧ください。 
################################################################################
#
# Note: 
#   - Revision history is available at 
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part1  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part2  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part3  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part4  
#
# 注記: 
#   - 更新履歴は以下からご覧いただけます。 
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part1  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part2  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part3  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part4  
#

がんくま
井二かける
### END AUTO-GENERATED LIST.  LEAVE THIS LINE HERE; SCRIPTS LOOK FOR IT. ここまで自動生成です。上記を編集しないでください ###
東條あいり	万能倉まな
えつむ	祝園アカネ
小豆戒斗	山家 宏佳
桜瀬　尋	英賀保芽依
那々海ゆあ	垂水 結菜
芋	少佐
岡松　仗	中舟生 良文
岡松　仗	八橋 次郎
空りんご	膳所
りゅうが	網干 茉莉
たんげゲンタ	山口
真希那稜	御来屋みく
中嶋有志	朝霧 義満
JORI（にじぽり委員会）	雲雀丘(車掌)
べ～	亀田
べ～	力士
桜瀬　尋	向山車掌
冷水優果	夢前さくら
H-0371	敦賀
矢塚　翔	敬川 康
浅沼諒空	月田 万作
べ～	葛城 岩男
髙咲　舞	鴻巣 燕(指令員)
香月	香登(指令員)
村尾祥平	余部 静夫
べ～	ガヤ
たんげゲンタ	ガヤ
西寺みふゆ	ガヤ
犬上太一	ガヤ
武川　楓	ガヤ
香月	ガヤ
上原一之龍（musica-ef）	音楽
雲丹	原曲
Melodic Taste	原曲
がんくま	音響効果,MIX
aosaki	OP曲ボーカル